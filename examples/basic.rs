use bevy_termion::prelude::*;

#[derive(Component)]
struct ShowKey;

fn main() {
    App::new()
        .add_plugin(TermionPlugin)
        .insert_resource(TermionConfig {
            frame_rate: 10,
            ..Default::default()
        })
        .add_startup_system(init)
        .add_system(show_keys)
        .run();
}

fn init(mut c: Commands) {
    c.spawn_bundle(TermTextBundle {
        text: "Hello world".into(),
        location: (0, 0).into(),
        ..Default::default()
    })
    .insert(ShowKey);

    c.spawn_bundle(TermTextBundle {
        text: "RED".into(),
        location: (10, 0).into(),
        style: TermStyle {
            fg_color: Some(color::Red.into()),
            ..Default::default()
        },
        ..Default::default()
    });

    c.spawn_bundle(TermTextBundle {
        text: "Green".into(),
        location: (11, 0).into(),
        style: TermStyle {
            fg_color: Some(color::Green.into()),
            bold: true,
            ..Default::default()
        },
        ..Default::default()
    });

    c.spawn_bundle(TermTextBundle {
        text: "Blue".into(),
        location: (12, 0).into(),
        style: TermStyle {
            bg_color: Some(color::Blue.into()),
            ..Default::default()
        },
        ..Default::default()
    });
}

fn show_keys(mut q: Query<&mut Text, With<ShowKey>>, mut events: EventReader<Event>) {
    q.get_single_mut().unwrap().0 = events
        .iter()
        .map(|e| format!("{:?}", &e))
        .collect::<String>();
}
