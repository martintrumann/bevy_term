use std::{
    io::{self, Read},
    sync::mpsc::{self, Receiver},
};

#[derive(Deref)]
pub struct Events(Receiver<u8>);

impl Read for Events {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut total = 0;
        loop {
            if total >= buf.len() {
                break;
            }
            match self.0.try_recv() {
                Ok(b) => {
                    buf[total] = b;
                    total += 1;
                }
                Err(_) => break,
            }
        }
        Ok(total)
    }
}

pub fn byte_buffer() -> Events {
    let (send, recv) = mpsc::channel();

    std::thread::spawn(move || {
        for b in termion::get_tty().unwrap().bytes() {
            if let Ok(b) = b {
                if send.send(b).is_err() {
                    return;
                }
            }
        }
    });

    Events(recv)
}
