use termion::color::*;

#[derive(Debug)]
pub enum Color {
    AnsiValue(AnsiValue),
    Rgb(Rgb),
}

use std::fmt;
impl termion::color::Color for Color {
    #[inline]
    fn write_fg(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::AnsiValue(i) => i.write_fg(f),
            Self::Rgb(i) => i.write_fg(f),
        }
    }

    #[inline]
    fn write_bg(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::AnsiValue(i) => i.write_bg(f),
            Self::Rgb(i) => i.write_bg(f),
        }
    }
}

impl From<AnsiValue> for Color {
    fn from(f: AnsiValue) -> Self {
        Self::AnsiValue(f)
    }
}

impl From<Rgb> for Color {
    fn from(f: Rgb) -> Self {
        Self::Rgb(f)
    }
}

macro_rules! derive_color {
    ($name:ident, $value:expr) => {
        impl From<$name> for Color {
            fn from(_: $name) -> Self {
                Self::AnsiValue(AnsiValue($value))
            }
        }
    };
}

derive_color!(Black, 0);
derive_color!(Red, 1);
derive_color!(Green, 2);
derive_color!(Yellow, 3);
derive_color!(Blue, 4);
derive_color!(Magenta, 5);
derive_color!(Cyan, 6);
derive_color!(White, 7);
derive_color!(LightBlack, 8);
derive_color!(LightRed, 9);
derive_color!(LightGreen, 10);
derive_color!(LightYellow, 11);
derive_color!(LightBlue, 12);
derive_color!(LightMagenta, 13);
derive_color!(LightCyan, 14);
derive_color!(LightWhite, 15);
