use bevy_ecs::prelude::*;

pub mod color;

#[derive(Bundle, Default)]
pub struct TermTextBundle {
    pub text: Text,
    pub location: Location,
    pub style: TermStyle,
}

#[derive(Component, Default)]
pub struct Text(pub String);
impl From<String> for Text {
    fn from(s: String) -> Self {
        Self(s)
    }
}

impl From<&str> for Text {
    fn from(s: &str) -> Self {
        Self(s.into())
    }
}

#[derive(Component, Default)]
pub struct Location(pub (u16, u16));
impl From<(u16, u16)> for Location {
    fn from(t: (u16, u16)) -> Self {
        Self(t)
    }
}

#[derive(Component)]
pub struct TermStyle {
    pub bold: bool,
    pub fg_color: Option<color::Color>,
    pub bg_color: Option<color::Color>,
}

impl Default for TermStyle {
    fn default() -> Self {
        Self {
            bold: false,
            fg_color: None,
            bg_color: None,
        }
    }
}
