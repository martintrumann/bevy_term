#[macro_use]
extern crate derive_more;

use bevy_app::{App, CoreStage, Events as AppEvents, Plugin};
use event::parse_event;

use std::{
    io::{self, Read, Stdout},
    thread,
    time::{Duration, Instant},
};

use termion::{
    cursor::HideCursor as HC,
    raw::{IntoRawMode, RawTerminal as RS},
    terminal_size,
};

mod event;
use event::{Event, Key};

mod render;
mod term_text;

mod async_events;
use async_events::byte_buffer;

pub mod prelude;

#[derive(Debug, Clone)]
pub struct TermionConfig {
    pub quit: Option<Event>,
    pub frame_rate: u8,
}
impl Default for TermionConfig {
    fn default() -> Self {
        Self {
            quit: Some(Event::Key(Key::Char('q'))),
            frame_rate: 60,
        }
    }
}

#[cfg(not(feature = "NoAs"))]
use termion::{input::MouseTerminal as MT, screen::AlternateScreen as AS};

#[cfg(not(feature = "NoAs"))]
pub(crate) type Output = AS<MT<HC<RS<Stdout>>>>;
#[cfg(feature = "NoAs")]
pub(crate) type Output = HC<RS<Stdout>>;

pub struct TermionWindow {
    pub size: (u16, u16),
    output: Output,
}

pub struct TermionPlugin;
impl Plugin for TermionPlugin {
    fn build(&self, app: &mut bevy_app::App) {
        let output = HC::from(io::stdout().into_raw_mode().unwrap());
        #[cfg(not(feature = "NoAs"))]
        let output = AS::from(MT::from(output));

        let size = terminal_size().unwrap();

        app.add_event::<Event>()
            .set_runner(termion_runner)
            .insert_resource(TermionWindow { size, output })
            .add_system_to_stage(CoreStage::PostUpdate, render::full_render);
    }
}

fn termion_runner(mut app: App) {
    let mut bytes = byte_buffer().bytes();

    let config = app
        .world
        .remove_resource::<TermionConfig>()
        .unwrap_or_default();
    let frame_rate = config.frame_rate;

    'm: loop {
        let frame_start = Instant::now();

        while let Some(Ok(byte)) = bytes.next() {
            let event = parse_event(byte, &mut bytes).unwrap();

            if Some(&event) == config.quit.as_ref() {
                break 'm;
            }

            let mut events = app.world.get_resource_mut::<AppEvents<Event>>().unwrap();

            events.send(event);
        }

        app.update();

        thread::sleep(
            Duration::from_millis((1000 / frame_rate as u16).into())
                .checked_sub(frame_start.elapsed())
                .unwrap_or(Duration::ZERO),
        )
    }
}
