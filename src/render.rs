use crate::prelude::*;
use termion::color;

fn render_items<'a>(
    items: impl Iterator<Item = (&'a Text, &'a Location, &'a TermStyle)>,
    screen: &mut ResMut<TermionWindow>,
) {
    for (text, Location((x, y)), style) in items {
        let _ = write!(screen.output, "{}", termion::cursor::Goto(*y + 1, *x + 1),);

        let mut content = text.0.clone();

        if style.bold {
            content = format!(
                "{}{}{}",
                termion::style::Bold,
                content,
                termion::style::NoBold
            )
        }

        if let Some(fg) = style.fg_color.as_ref() {
            content = format!(
                "{}{}{}",
                color::Fg(fg as &dyn color::Color),
                content,
                color::Fg(color::Reset)
            )
        }

        if let Some(bg) = style.bg_color.as_ref() {
            content = format!(
                "{}{}{}",
                color::Bg(bg as &dyn color::Color),
                content,
                color::Bg(color::Reset)
            )
        }

        let _ = write!(screen.output, "{}", content);
    }
}

pub fn full_render(q: Query<(&Text, &Location, &TermStyle)>, mut screen: ResMut<TermionWindow>) {
    let _ = write!(screen.output, "{}", termion::clear::All);

    render_items(q.iter(), &mut screen);

    let _ = screen.output.flush();
}

pub fn render_changed(
    q: Query<
        (&Text, &Location, &TermStyle),
        Or<(Changed<Text>, Changed<Location>, Changed<TermStyle>)>,
    >,
    mut screen: ResMut<TermionWindow>,
) {
    render_items(q.iter(), &mut screen);

    let _ = screen.output.flush();
}
